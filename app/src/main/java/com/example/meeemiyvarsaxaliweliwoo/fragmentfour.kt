package com.example.meeemiyvarsaxaliweliwoo

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment

class fragmentfour : Fragment(R.layout.fragment4) {
    private lateinit var buttonEnter: Button
    private lateinit var editTextName: EditText
    private lateinit var textView: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonEnter = view.findViewById(R.id.buttonEnter)
        editTextName = view.findViewById(R.id.editTextName)
        textView = view.findViewById(R.id.textView5)


        val sharedPreferences =
            requireActivity().getSharedPreferences("my_notes_pref", Context.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTE", "")
        textView.text = text


        buttonEnter.setOnClickListener {
            val name = editTextName.text.toString()
            val text = textView.text.toString()
            val result = name + "\n" + text
            textView.text = result
            sharedPreferences.edit()
                .putString("NOTE", result)
                .apply()

        }

    }
}