package com.example.meeemiyvarsaxaliweliwoo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout:TabLayout
    private lateinit var viewPagerFragAd: ViewPagerFragAd



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.viewPager)
        tabLayout = findViewById(R.id.tabLayout)
        viewPagerFragAd = ViewPagerFragAd(this)
        viewPager.adapter = viewPagerFragAd
        TabLayoutMediator(tabLayout,viewPager){tab,position ->
            tab.text = "tab ${position + 1}"

        }.attach()
    }
}